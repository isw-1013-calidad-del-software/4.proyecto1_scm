﻿namespace ScheduleControlManagement
{
    partial class SCM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Tab_Inicio = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgSchedule = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.lbl_memberline = new System.Windows.Forms.Label();
            this.lbl_lastname = new System.Windows.Forms.Label();
            this.lbl_firstname = new System.Windows.Forms.Label();
            this.tbx_idPartner = new System.Windows.Forms.TextBox();
            this.lbl_idpartner = new System.Windows.Forms.Label();
            this.dgPartner = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tbx_fisrtname = new System.Windows.Forms.TextBox();
            this.tbx_lastname = new System.Windows.Forms.TextBox();
            this.txb_memberline = new System.Windows.Forms.TextBox();
            this.cbx_status = new System.Windows.Forms.CheckBox();
            this.btn_new = new System.Windows.Forms.Button();
            this.btn_edit = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_search = new System.Windows.Forms.Button();
            this.Tab_Inicio.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSchedule)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPartner)).BeginInit();
            this.SuspendLayout();
            // 
            // Tab_Inicio
            // 
            this.Tab_Inicio.AccessibleDescription = "ff";
            this.Tab_Inicio.AccessibleName = "Inicio";
            this.Tab_Inicio.Controls.Add(this.tabPage1);
            this.Tab_Inicio.Controls.Add(this.tabPage2);
            this.Tab_Inicio.Controls.Add(this.tabPage3);
            this.Tab_Inicio.Controls.Add(this.tabPage4);
            this.Tab_Inicio.Controls.Add(this.tabPage5);
            this.Tab_Inicio.Location = new System.Drawing.Point(3, 0);
            this.Tab_Inicio.Name = "Tab_Inicio";
            this.Tab_Inicio.SelectedIndex = 0;
            this.Tab_Inicio.Size = new System.Drawing.Size(675, 377);
            this.Tab_Inicio.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.AccessibleName = "";
            this.tabPage1.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(667, 351);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Inicio";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgSchedule);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(667, 351);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Horarios";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgSchedule
            // 
            this.dgSchedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSchedule.Location = new System.Drawing.Point(6, 101);
            this.dgSchedule.Name = "dgSchedule";
            this.dgSchedule.Size = new System.Drawing.Size(655, 247);
            this.dgSchedule.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(667, 351);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Recorridos";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btn_search);
            this.tabPage4.Controls.Add(this.btn_delete);
            this.tabPage4.Controls.Add(this.btn_edit);
            this.tabPage4.Controls.Add(this.btn_new);
            this.tabPage4.Controls.Add(this.cbx_status);
            this.tabPage4.Controls.Add(this.txb_memberline);
            this.tabPage4.Controls.Add(this.tbx_lastname);
            this.tabPage4.Controls.Add(this.tbx_fisrtname);
            this.tabPage4.Controls.Add(this.lbl_memberline);
            this.tabPage4.Controls.Add(this.lbl_lastname);
            this.tabPage4.Controls.Add(this.lbl_firstname);
            this.tabPage4.Controls.Add(this.tbx_idPartner);
            this.tabPage4.Controls.Add(this.lbl_idpartner);
            this.tabPage4.Controls.Add(this.dgPartner);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(667, 351);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Socios";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // lbl_memberline
            // 
            this.lbl_memberline.AutoSize = true;
            this.lbl_memberline.Location = new System.Drawing.Point(27, 100);
            this.lbl_memberline.Name = "lbl_memberline";
            this.lbl_memberline.Size = new System.Drawing.Size(33, 13);
            this.lbl_memberline.TabIndex = 5;
            this.lbl_memberline.Text = "Linea";
            this.lbl_memberline.Click += new System.EventHandler(this.Label3_Click);
            // 
            // lbl_lastname
            // 
            this.lbl_lastname.AutoSize = true;
            this.lbl_lastname.Location = new System.Drawing.Point(27, 71);
            this.lbl_lastname.Name = "lbl_lastname";
            this.lbl_lastname.Size = new System.Drawing.Size(44, 13);
            this.lbl_lastname.TabIndex = 4;
            this.lbl_lastname.Text = "Apellido";
            // 
            // lbl_firstname
            // 
            this.lbl_firstname.AutoSize = true;
            this.lbl_firstname.Location = new System.Drawing.Point(27, 45);
            this.lbl_firstname.Name = "lbl_firstname";
            this.lbl_firstname.Size = new System.Drawing.Size(44, 13);
            this.lbl_firstname.TabIndex = 3;
            this.lbl_firstname.Text = "Nombre";
            // 
            // tbx_idPartner
            // 
            this.tbx_idPartner.Location = new System.Drawing.Point(73, 13);
            this.tbx_idPartner.Name = "tbx_idPartner";
            this.tbx_idPartner.Size = new System.Drawing.Size(100, 20);
            this.tbx_idPartner.TabIndex = 2;
            // 
            // lbl_idpartner
            // 
            this.lbl_idpartner.AutoSize = true;
            this.lbl_idpartner.Location = new System.Drawing.Point(27, 16);
            this.lbl_idpartner.Name = "lbl_idpartner";
            this.lbl_idpartner.Size = new System.Drawing.Size(40, 13);
            this.lbl_idpartner.TabIndex = 1;
            this.lbl_idpartner.Text = "Código";
            // 
            // dgPartner
            // 
            this.dgPartner.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPartner.Location = new System.Drawing.Point(5, 141);
            this.dgPartner.Name = "dgPartner";
            this.dgPartner.Size = new System.Drawing.Size(659, 210);
            this.dgPartner.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(667, 351);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Buses";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tbx_fisrtname
            // 
            this.tbx_fisrtname.Location = new System.Drawing.Point(73, 42);
            this.tbx_fisrtname.Name = "tbx_fisrtname";
            this.tbx_fisrtname.Size = new System.Drawing.Size(163, 20);
            this.tbx_fisrtname.TabIndex = 6;
            // 
            // tbx_lastname
            // 
            this.tbx_lastname.Location = new System.Drawing.Point(73, 68);
            this.tbx_lastname.Name = "tbx_lastname";
            this.tbx_lastname.Size = new System.Drawing.Size(163, 20);
            this.tbx_lastname.TabIndex = 7;
            // 
            // txb_memberline
            // 
            this.txb_memberline.Location = new System.Drawing.Point(73, 97);
            this.txb_memberline.Name = "txb_memberline";
            this.txb_memberline.Size = new System.Drawing.Size(163, 20);
            this.txb_memberline.TabIndex = 8;
            // 
            // cbx_status
            // 
            this.cbx_status.AutoSize = true;
            this.cbx_status.Location = new System.Drawing.Point(180, 19);
            this.cbx_status.Name = "cbx_status";
            this.cbx_status.Size = new System.Drawing.Size(56, 17);
            this.cbx_status.TabIndex = 9;
            this.cbx_status.Text = "Activo";
            this.cbx_status.UseVisualStyleBackColor = true;
            // 
            // btn_new
            // 
            this.btn_new.Location = new System.Drawing.Point(589, 6);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(75, 23);
            this.btn_new.TabIndex = 10;
            this.btn_new.Text = "Nuevo";
            this.btn_new.UseVisualStyleBackColor = true;
            // 
            // btn_edit
            // 
            this.btn_edit.Location = new System.Drawing.Point(589, 66);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(75, 23);
            this.btn_edit.TabIndex = 11;
            this.btn_edit.Text = "Editar";
            this.btn_edit.UseVisualStyleBackColor = true;
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(589, 94);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_delete.TabIndex = 12;
            this.btn_delete.Text = "Eliminar";
            this.btn_delete.UseVisualStyleBackColor = true;
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(589, 35);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.TabIndex = 13;
            this.btn_search.Text = "Buscar";
            this.btn_search.UseVisualStyleBackColor = true;
            // 
            // SCM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 389);
            this.Controls.Add(this.Tab_Inicio);
            this.Name = "SCM";
            this.Text = "Gestion de control de horarios";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Tab_Inicio.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSchedule)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPartner)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl Tab_Inicio;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dgSchedule;
        private System.Windows.Forms.Label lbl_memberline;
        private System.Windows.Forms.Label lbl_lastname;
        private System.Windows.Forms.Label lbl_firstname;
        private System.Windows.Forms.TextBox tbx_idPartner;
        private System.Windows.Forms.Label lbl_idpartner;
        private System.Windows.Forms.DataGridView dgPartner;
        private System.Windows.Forms.TextBox txb_memberline;
        private System.Windows.Forms.TextBox tbx_lastname;
        private System.Windows.Forms.TextBox tbx_fisrtname;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.CheckBox cbx_status;
    }
}

