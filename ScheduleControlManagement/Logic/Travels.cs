﻿using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Relational;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScheduleControlManagement
{
    class Travels
    {
        //Inicializa conexion a BD
        public ConnectDB connect = new ConnectDB();
        public MySqlDataAdapter da;
        public DataTable dt;
        public DataRow dr;
        public static MySqlConnection conn = ConnectDB.MySqlConnect();
        
        public Travels()
        {

        }
        //Funcion para agregar viajes
        public string Insert_Travel(int bus_id, string description, int delay_time, string route_area, int total_travels)
        {
            string result = "Exito";
            if (delay_time < 0 || total_travels < 0 || description.Equals("") || route_area.Equals(""))
            {
                result = "Error: Datos incorrectos";
            }
            else
            {
                string query = "INSERT INTO travels (bus_id , description , delay_time , route_area, total_travels , state ) " +
                "VALUES(" + bus_id + ",'" + description + "'," + delay_time + ", '" + route_area + "' , "+ total_travels +", 1);";
                MySqlCommand cmd = new MySqlCommand(query, conn);

                try
                {
                    conn.Close();
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    result = "Error!: " + ex.Message;
                }
            }
            
            return result;
        }
        //Funcion para retornar un numero aleatorio de viaje, para pruebas
        public int Random_idTravel()
        {
            conn.Close();
            int result = -1;
            String query = "select idTravel from deb.travels where state = 1 order by rand() limit 1;";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            try
            {
                //conn.Close();
                conn.Open();
                cmd.ExecuteNonQuery();
                result = (Int32)cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return result;
        }

        public object[,] SearchTravel(int searchId, string searchColumn, string state)
        {

            String query = "select * from deb.travels where "+ searchColumn +" = "+searchId+" and state like '"+state+"';";
            dt = new DataTable();
            da = new MySqlDataAdapter(query, conn);
            da.Fill(dt);
            int cont = 0;
            object[,] result;


            if (dt.Rows.Count > 0)
            {
                result = new object[dt.Rows.Count, dt.Columns.Count];

                foreach (DataRow row in dt.Rows)
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        result[cont, i] = row[i];
                    }
                    cont++;
                }
            } else
            {
                result = new object[1, 1];
                result[0, 0] = false;
            }
            
            return result;
        }

        public string Update_Travel(int idTravel, int bus_id, string description, int delay_time, string route_area, int total_travels, string searchState)
        {
            string result = "Error";
            string searchColumn = "idTravel";      //Columna para discriminar
            
            //Valida si valores no aceptados
            if (delay_time < 0 || total_travels < 0 || description.Equals("") || route_area.Equals(""))
            {
                result = "Error: Datos incorrectos";
            }
            else
            {
                //Busca el registro por actualizar
                Travels trv = new Travels();
                object[,] row_for_update = trv.SearchTravel(idTravel, searchColumn, searchState);

                //Verifica que encontrara el registro
                if (idTravel.Equals(row_for_update[0,0]))
                {
                    //Si el registro exite lo actualiza
                    string query = "UPDATE travels SET " +
                                        "bus_id =  " + bus_id + ", " +
                                        "description =  '" + description + "', " +
                                        "delay_time =  " + delay_time + ", " +
                                        "route_area =  '" + route_area + "', " +
                                        "total_travels =  " + total_travels + 
                                    " WHERE idTravel =  " + idTravel + "; ";

                    MySqlCommand cmd = new MySqlCommand(query, conn);

                    try
                    {
                        conn.Close();
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        result = "Exito";
                    }
                    catch (Exception ex)
                    {
                        result += ex.Message;
                    }
                }
                else
                {
                    result += " Registro no encontrado";
                }
            }

            return result;
        }

        public static string Change_State_Travel(int idTravel, int state)
        {
            string result = "Error";
            string searchColumn = "idTravel";      //Columna para discriminar
            string searchState = "%";

            //Valida si valores no aceptados
            if (state < 0 || state > 1)
            {
                result = "Error: Datos incorrectos";
            }
            else
            {
                //Busca el registro por actualizar
                Travels trv = new Travels();
                object[,] row_for_update = trv.SearchTravel(idTravel, searchColumn, searchState);

                //Verifica que encontrara el registro
                if (!row_for_update[0, 0].Equals(false))
                {
                    //Si el registro exite lo actualiza
                    string query = "UPDATE travels SET " +
                                        "state =  " + state +
                                   " WHERE idTravel =  " + idTravel + "; ";

                    MySqlCommand cmd = new MySqlCommand(query, conn);

                    try
                    {
                        conn.Close();
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        result = "Exito";
                    }
                    catch (Exception ex)
                    {
                        result += ex.Message;
                    }
                } 
                else
                {
                    result += "Registro no encontrado";
                }
            }

            return result;
        }
    }
}
