﻿using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleControlManagement.Logic
{

    class Bus
    {
        //Inicializa conexion a BD
        public ConnectDB connect = new ConnectDB();
        public MySqlDataAdapter da;
        public DataTable dt;
        public static MySqlConnection conn = ConnectDB.MySqlConnect();
        public Bus()
        {
            
        }

        public object Read_Bus()
        {
            String query = "SELECT * FROM bus;";
            dt = new DataTable();
            da = new MySqlDataAdapter(query, conn);
            da.Fill(dt);
            return dt;
        }

        public int Random_idBus()
        {
            int result = -1;
            String query = "select idBus from deb.bus order by rand();";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            try
            {
                conn.Close();
                conn.Open();
                cmd.ExecuteNonQuery();
                result = (Int32)cmd.ExecuteScalar();
                
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return result;
        }

        public static string Insert_Bus(int partnerId, int capacity, string driver, string helper)
        {
            conn.Close();
            string result = "Exito";
            string query = "INSERT INTO bus (partnerId,capacity,driver,helper,status) " +
                "VALUES ('"+ partnerId + "','" + capacity + "','" + driver + "','" + helper + "', 1); ";
            MySqlCommand cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            } catch (Exception ex)
            {
                result = "Error!: " + ex.Message;
            }
            return result;
        }
    }
}
