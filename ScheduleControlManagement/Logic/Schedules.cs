﻿using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Relational;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleControlManagement
{
    class Schedules
    {
        //Inicializa conexion a BD
        public ConnectDB connect = new ConnectDB();
        public MySqlDataAdapter da;
        public DataTable dt;
        public DataRow dr;
        public static MySqlConnection conn = ConnectDB.MySqlConnect();
        
        public Schedules()
        {

        }

        public static string Insert_Schedule(int bus_id, string description, int start_time, int end_time)
        {
            string result = "Exito";
            if (start_time < 0 || end_time < 0 || description.Equals(""))
            {
                result = "Error: Horas ingresadas negativas";
            }
            else
            {
                string query = "INSERT INTO schedules (bus_id , description , start_time , end_time , state ) " +
                "VALUES(" + bus_id + ",'" + description + "'," + start_time + "," + end_time + ", 1);";
                MySqlCommand cmd = new MySqlCommand(query, conn);

                try
                {
                    conn.Close();
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    result = "Error!: " + ex.Message;
                }
            }
            
            return result;
        }

        public int Random_idSchedule()
        {
            conn.Close();
            int result = -1;
            //String query = "select idSchedules from deb.schedules order by rand();";
            String query = "select idSchedules from deb.schedules where state = 1 order by rand() limit 1;";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            try
            {
                //conn.Close();
                conn.Open();
                cmd.ExecuteNonQuery();
                result = (Int32)cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return result;
        }

        public object[,] SearchSchedule(int searchId, string searchColumn, string state)
        {

            String query = "select * from deb.schedules where "+ searchColumn +" = "+searchId+" and state like '"+state+"';";
            dt = new DataTable();
            da = new MySqlDataAdapter(query, conn);
            da.Fill(dt);
            int cont = 0;
            object[,] result;


            if (dt.Rows.Count > 0)
            {
                result = new object[dt.Rows.Count, dt.Columns.Count];

                foreach (DataRow row in dt.Rows)
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        result[cont, i] = row[i];
                    }
                    cont++;
                }
            } else
            {
                result = new object[1, 1];
                result[0, 0] = false;
            }
            
            return result;
        }

        public static string Update_Schedule(int idSchedule, int bus_id, string description, int start_time, int end_time, string searchState)
        {
            string result = "Error";
            string searchColumn = "idSchedules";      //Columna para discriminar
            
            //Valida si valores no aceptados
            if (start_time < 0 || end_time < 0 || description.Equals(""))
            {
                result = "Error: Datos incorrectos";
            }
            else
            {
                //Busca el registro por actualizar
                Schedules sch = new Schedules();
                object[,] row_for_update = sch.SearchSchedule(idSchedule, searchColumn, searchState);

                //Verifica que encontrara el registro
                if (idSchedule.Equals(row_for_update[0,0]))
                {
                    //Si el registro exite lo actualiza
                    string query = "UPDATE schedules SET " +
                                        "bus_id =  " + bus_id + ", " +
                                        "description =  '" + description + "', " +
                                        "start_time =  " + start_time + ", " +
                                        "end_time =  " + end_time + 
                                    " WHERE idSchedules =  " + idSchedule + "; ";

                    MySqlCommand cmd = new MySqlCommand(query, conn);

                    try
                    {
                        conn.Close();
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        result = "Exito";
                    }
                    catch (Exception ex)
                    {
                        result += ex.Message;
                    }
                }
                else
                {
                    result += " Registro no encontrado";
                }
            }

            return result;
        }

        public static string Change_State_Schedule(int idSchedule, int state)
        {
            string result = "Error";
            string searchColumn = "idSchedules";      //Columna para discriminar
            string searchState = "%";

            //Valida si valores no aceptados
            if (state < 0 || state > 1)
            {
                result = "Error: Datos incorrectos";
            }
            else
            {
                //Busca el registro por actualizar
                Schedules sch = new Schedules();
                object[,] row_for_update = sch.SearchSchedule(idSchedule, searchColumn, searchState);

                //Verifica que encontrara el registro
                //if (idSchedule.ToString().Equals(row_for_update[0,0]))
                if (!row_for_update[0, 0].Equals(false))
                {
                    //Si el registro exite lo actualiza
                    string query = "UPDATE schedules SET " +
                                        "state =  " + state +
                                   " WHERE idSchedules =  " + idSchedule + "; ";

                    MySqlCommand cmd = new MySqlCommand(query, conn);

                    try
                    {
                        conn.Close();
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        result = "Exito";
                    }
                    catch (Exception ex)
                    {
                        result += ex.Message;
                    }
                } 
                else
                {
                    result += "Registro no encontrado";
                }
            }

            return result;
        }
    }
}
