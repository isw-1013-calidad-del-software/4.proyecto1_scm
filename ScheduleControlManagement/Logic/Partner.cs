﻿using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleControlManagement.Logic
{

    class Partner
    {
        //Inicializa conexion a BD
        public ConnectDB connect = new ConnectDB();
        public MySqlDataAdapter da;
        public DataTable dt;
        public static MySqlConnection conn = ConnectDB.MySqlConnect();
        public static int LastInsertedId;
        public Partner()
        {

        }

        public object Read_Partners()
        {
            String query = "SELECT * FROM partner;";
            dt = new DataTable();
            da = new MySqlDataAdapter(query, conn);
            da.Fill(dt);
            return dt;
        }

        public static string Insert_Partner(string firstname, string lastname, string member_line)
        {
            string result = "Exito";
            if (firstname.Equals("") || lastname.Equals("") || member_line.Equals(""))
            {
                result = "Error: Valores nulos";
            } else
            {
                conn.Close();
                result = "Exito";
                string query = "INSERT INTO partner (firstname, lastname, member_line, status) " +
                    "VALUES ('" + firstname + "', '" + lastname + "', '" + member_line + "', 1 ); ";
                MySqlCommand cmd = new MySqlCommand(query, conn);

                try
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    LastInsertedId = (int)cmd.LastInsertedId;

                }
                catch (Exception ex)
                {
                    result = "Error!: " + ex.Message;
                }
            }
            return result;
        }

 
    }
}
