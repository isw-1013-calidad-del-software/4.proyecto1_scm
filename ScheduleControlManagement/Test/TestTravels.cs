﻿using MySqlX.XDevAPI.Common;
using NUnit.Framework;
using ScheduleControlManagement.Logic;
using System;

namespace ScheduleControlManagement
{
    [TestFixture]
    class TestTravels
    {
        Bus bus = new Bus();
        Travels trv = new Travels();
        Random rnd = new Random();

        // Pruebas de registro de horarios exitosa
        [Test]
        public void Test6_RecordTravels1()
        {
            //Arrange
            int bus_id = bus.Random_idBus(); //generara un random entre los id de buses
            string description = "description test 1";
            int delay_time = rnd.Next(1, 3);//numero aleatorio para la hora de salida
            string route_area = "ruta test 1";
            int total_travels = rnd.Next(1, 10); //numero aleatorio entre 1 y 10 del total de viajes

            //Act
            string result = trv.Insert_Travel(bus_id, description, delay_time, route_area, total_travels);

            //Assert
            Assert.AreEqual("Exito", result);
        }

        // Pruebas de registro de horarios fallida por datos negativos
        [Test]
        public void Test6_RecordTravels2()
        {
            //Arrange
            int bus_id = bus.Random_idBus(); //generara un random entre los id de buses
            string description = "description test 1";
            int delay_time = rnd.Next(1, 3);//numero aleatorio para la hora de salida
            string route_area = "ruta test 1";
            int total_travels = -10; //numero negativo para generar error

            //Act
            string result = trv.Insert_Travel(bus_id, description, delay_time, route_area, total_travels);

            //Assert
            Assert.AreNotEqual("Exito", result);
        }

        // Pruebas de registro de horarios fallida por datos negativos
        [Test]
        public void Test6_RecordTravels3()
        {
            //Arrange
            int bus_id = -5; //Usa un id que no exite en la tabla buses
            string description = "description test 1";
            int delay_time = rnd.Next(1, 3);//numero aleatorio para la hora de salida
            string route_area = "ruta test 1";
            int total_travels = rnd.Next(1, 10); //numero aleatorio entre 1 y 3 horas despues de la salida

            //Act
            string result = trv.Insert_Travel(bus_id, description, delay_time, route_area, total_travels);

            //Assert
            Assert.AreNotEqual("Exito", result);
        }

        // Pruebas de busqueda de horario exitosa
        [Test]
        public void Test7_SearchTravel1()
        {
            //Arange
            int idTravel = trv.Random_idTravel(); //generara un random entre los id de viajes
            string searchColumn = "idTravel";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] result = trv.SearchTravel(idTravel, searchColumn, searchState);

            //Assert
            Assert.AreEqual(idTravel, result[0,0]);
        }

        // Pruebas de busqueda de horario fallida
        [Test]
        public void Test7_SearchTravel2()
        {
            //Arange
            int idTravel = -5; //generara un random entre los id de horarios
            string searchColumn = "idTravel";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] result = trv.SearchTravel(idTravel, searchColumn, searchState);

            //Assert
            Assert.AreNotEqual(idTravel.ToString(), result[0,0]);
        }

        // Pruebas de busqueda de horario fallida
        [Test]
        public void Test7_SearchTravel3()
        {
            //Arange
            int idTravel = 0; //id invalido
            string searchColumn = "idTravel";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] result = trv.SearchTravel(idTravel, searchColumn, searchState);

            //Assert
            Assert.AreNotEqual(idTravel.ToString(), result[0,0]);
        }

        // Pruebas de actualizacion de registro de horarios exitosa, validando datos
        [Test]
        public void Test8_UpdateTravel1()
        {
            //Arrange
            int idTravel = trv.Random_idTravel(); //generara un random entre los id de viajes
            int bus_id = bus.Random_idBus(); //generara un random entre los id de buses
            string description = idTravel + " actualizado viaje del bus " + bus_id;
            int delay_time = rnd.Next(1, 3);//numero aleatorio para la hora de salida
            string route_area = "ruta test 1";
            int total_travels = rnd.Next(1, 10); //numero aleatorio entre 1 y 10 del total de viajes
            int state = 1;
            string searchColumn = "idTravel";      //Columna para discriminar
            string searchState = "%";

            //Act
            string result = trv.Update_Travel(idTravel, bus_id, description, delay_time, route_area, total_travels, searchState);
            if (result.Equals("Exito"))
            {
                object[,] row_update = trv.SearchTravel(idTravel, searchColumn, searchState);
                
                if ((row_update[0,1].Equals(bus_id) ) &&
                    (row_update[0,2].Equals(description) ) &&
                    (row_update[0,3].Equals(delay_time) ) &&
                    (row_update[0,4].Equals(route_area) ) &&
                    (row_update[0,5].Equals(total_travels)) &&
                    (row_update[0,6].Equals(state) ) 
                   )
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreEqual("Exito2", result);
        }

        // Pruebas de actualizacion de registro de horarios exitosa, validando datos
        [Test]
        public void Test8_UpdateTravel2()
        {
            //Arrange
            int idTravel = trv.Random_idTravel(); //generara un random entre los id de horarios
            int bus_id = bus.Random_idBus(); //generara un random entre los id de buses
            string description = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            int delay_time = rnd.Next(1, 3);//numero aleatorio para la duracion del viaje
            string route_area = "ruta test 1";
            int total_travels = rnd.Next(1, 10); //numero aleatorio entre 1 y 10 del total de viajes
            int state = 1;
            string searchColumn = "travels";      //Columna para discriminar
            string searchState = "%";

            //Act
            string result = trv.Update_Travel(idTravel, bus_id, description, delay_time, route_area, total_travels, searchState);
            if (result.Equals("Exito"))
            {
                object[,] row_update = trv.SearchTravel(idTravel, searchColumn, searchState);

                if ((row_update[0, 1].Equals(bus_id.ToString())) &&
                    (row_update[0, 2].Equals(description.ToString())) &&
                    (row_update[0, 3].Equals(delay_time)) &&
                    (row_update[0, 4].Equals(route_area)) &&
                    (row_update[0, 5].Equals(total_travels)) &&
                    (row_update[0, 6].Equals(state))
                   )
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreNotEqual("Exito2", result);
        }

        // Pruebas de actualizacion de registro de horarios exitosa, validando datos
        [Test]
        public void Test8_UpdateTravel3()
        {
            //Arrange
            int idTravel = -5; //generara un random entre los id de horarios
            int bus_id = bus.Random_idBus(); //generara un random entre los id de buses
            string description = idTravel + " actualizado update:" + bus_id;
            int delay_time = rnd.Next(1, 3);//numero aleatorio para la duracion del viaje
            string route_area = "ruta test 1";
            int total_travels = rnd.Next(1, 10); //numero aleatorio entre 1 y 10 del total de viajes
            int state = rnd.Next(1, 2);
            string searchColumn = "travels";      //Columna para discriminar
            string searchState = "%";

            //Act
            string result = trv.Update_Travel(idTravel, bus_id, description, delay_time, route_area, total_travels, searchState);
            if (result.Equals("Exito"))
            {
                object[,] row_update = trv.SearchTravel(idTravel, searchColumn, searchState);

                if ((row_update[0, 1].Equals(bus_id.ToString())) &&
                    (row_update[0, 2].Equals(description.ToString())) &&
                    (row_update[0, 3].Equals(delay_time)) &&
                    (row_update[0, 4].Equals(route_area)) &&
                    (row_update[0, 5].Equals(total_travels)) &&
                    (row_update[0, 6].Equals(state))
                   )
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreNotEqual("Exito2", result);
        }

        // Pruebas de actualizacion de estado de horarios exitosa, validando datos
        [Test]
        public void Test9_DisableTravel1()
        {
            //Arrange
            int idTravel = trv.Random_idTravel(); //generara un random entre los id de viajes
            int state = 0;//inactivo
            string searchColumn = "idTravel";      //Columna para discriminar
            string searchState = "0";

            //Act
            string result = Travels.Change_State_Travel(idTravel, state);
            if (result.Equals("Exito"))
            {
                object[,] row_update = trv.SearchTravel(idTravel, searchColumn, searchState);

                if (row_update[0,6].Equals(state))
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreEqual("Exito2", result);
        }

        // Pruebas de actualizacion de estado de horarios fallida por estados
        [Test]
        public void Test9_DisableTravel2()
        {
            //Arrange
            int idTravel = -55; //id de horario inexistente
            int state = 0;//inactivo
            string searchColumn = "idTravel";      //Columna para discriminar
            string searchState = "0";

            //Act
            string result = Schedules.Change_State_Schedule(idTravel, state);
            if (result.Equals("Exito2"))
            {
                object[,] row_update = trv.SearchTravel(idTravel, searchColumn, searchState);

                if (row_update[0, 6].Equals(state.ToString()))
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreNotEqual("Exito2", result);
        }

        // Pruebas de actualizacion de estado de horarios fallida por estado, validando datos
        [Test]
        public void Test9_DisableTravel3()
        {
            //Arrange
            int idTravel = trv.Random_idTravel(); //generara un random entre los id de viajes
            int state = 2;//valor incorrecto
            string searchColumn = "idTravel";      //Columna para discriminar
            string searchState = "0";

            //Act
            string result = Schedules.Change_State_Schedule(idTravel, state);
            if (result.Equals("Exito2"))
            {
                object[,] row_update = trv.SearchTravel(idTravel, searchColumn, searchState);

                if (row_update[0, 6].Equals(state))
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreNotEqual("Exito1", result);
        }

        // Pruebas de busqueda de viajes por unidad de bus que tiene resultados
        [Test]
        public void Test10_SearchBusSchedule1()
        {
            //Arange
            int idTravel = trv.Random_idTravel(); //generara un random entre los id de horarios
            string searchColum1 = "idTravel";
            string searchColum2 = "bus_id";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] randomShedule = trv.SearchTravel(idTravel, searchColum1, searchState);
            int idBus = (int)randomShedule[0, 1]; //Me aseguro que la prueba tenga resultado
            object[,] result = trv.SearchTravel(idBus, searchColum2, searchState);

            //Assert
            Assert.AreNotEqual(false, result[0,0]);
        }

        // Pruebas de busqueda de viajes por unidad de bus que no tiene resultados
        [Test]
        public void Test10_SearchBusSchedule2()
        {
            //Arange
            int idBus = 1;
            string searchColum2 = "bus_id";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] result = trv.SearchTravel(idBus, searchColum2, searchState);

            //Assert
            Assert.AreEqual(false, result[0, 0]);
        }

        // Pruebas de busqueda de viejas por unidad de bus que no exite
        [Test]
        public void Test10_SearchBusSchedule3()
        {
            //Arange
            int idBus = -5;
            string searchColum2 = "bus_id";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] result = trv.SearchTravel(idBus, searchColum2, searchState);

            //Assert
            Assert.AreEqual(false, result[0, 0]);
        }
    }
}
