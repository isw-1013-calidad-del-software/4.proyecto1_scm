﻿using MySqlX.XDevAPI.Common;
using NUnit.Framework;
using ScheduleControlManagement.Logic;
using System;

namespace ScheduleControlManagement
{
    [TestFixture]
    class TestSchedules
    {
        Bus bus = new Bus();
        Schedules sch = new Schedules();
        Random rnd = new Random();

        // Pruebas de registro de horarios exitosa
        [Test]
        public void Test1_RecordSchedules1()
        {
            //Arrange
            int bus_id = bus.Random_idBus(); //generara un random entre los id de buses
            string description = "description test 1";
            int start_time = rnd.Next(8, 22);//numero aleatorio para la hora de salida
            int end_time = start_time + rnd.Next(1, 3); //numero aleatorio entre 1 y 3 horas despues de la salida

            //Act
            string result = Schedules.Insert_Schedule(bus_id, description, start_time, end_time);

            //Assert
            Assert.AreEqual("Exito", result);
        }

        // Pruebas de registro de horarios fallida por datos negativos
        [Test]
        public void Test1_RecordSchedules2()
        {
            //Arrange
            int bus_id = bus.Random_idBus(); //generara un random entre los id de buses
            string description = "description test 1";
            int start_time = rnd.Next(8, 22);//numero aleatorio para la hora de salida
            int end_time = -5; //

            //Act
            string result = Schedules.Insert_Schedule(bus_id, description, start_time, end_time);

            //Assert
            Assert.AreNotEqual("Exito", result);
        }

        // Pruebas de registro de horarios fallida por datos negativos
        [Test]
        public void Test1_RecordSchedules3()
        {
            //Arrange
            int bus_id = Partner.LastInsertedId + 1000000; //Usa un id que no exite en la tabla buses
            string description = "description test 1";
            int start_time = rnd.Next(8, 22);//numero aleatorio para la hora de salida
            int end_time = start_time + rnd.Next(1, 3); //numero aleatorio entre 1 y 3 horas despues de la salida

            //Act
            string result = Schedules.Insert_Schedule(bus_id, description, start_time, end_time);

            //Assert
            Assert.AreNotEqual("Exito", result);
        }

        // Pruebas de busqueda de horario exitosa
        [Test]
        public void Test2_SearchSchedule1()
        {
            //Arange
            int idSchedule = sch.Random_idSchedule(); //generara un random entre los id de horarios
            string searchColumn = "idSchedules";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] result = sch.SearchSchedule(idSchedule, searchColumn, searchState);

            //Assert
            Assert.AreEqual(idSchedule, result[0,0]);
        }

        // Pruebas de busqueda de horario fallida
        [Test]
        public void Test2_SearchSchedule2()
        {
            //Arange
            int idSchedule = -5; //generara un random entre los id de horarios
            string searchColumn = "idSchedules";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] result = sch.SearchSchedule(idSchedule, searchColumn, searchState);

            //Assert
            Assert.AreNotEqual(idSchedule.ToString(), result[0,0]);
        }

        // Pruebas de busqueda de horario fallida
        [Test]
        public void Test2_SearchSchedule3()
        {
            //Arange
            int idSchedule = 0; //id invalido
            string searchColumn = "idSchedules";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] result = sch.SearchSchedule(idSchedule, searchColumn, searchState);

            //Assert
            Assert.AreNotEqual(idSchedule.ToString(), result[0,0]);
        }

        // Pruebas de actualizacion de registro de horarios exitosa, validando datos
        [Test]
        public void Test3_UpdateSchedules1()
        {
            //Arrange
            int idSchedule = sch.Random_idSchedule(); //generara un random entre los id de horarios
            int bus_id = bus.Random_idBus(); //generara un random entre los id de buses
            string description = idSchedule + " description update:" + bus_id;
            int start_time = rnd.Next(8, 22);//numero aleatorio para la hora de salida
            int end_time = start_time + rnd.Next(1, 3); //numero aleatorio entre 1 y 3 horas despues de la salida
            int state = rnd.Next(1, 2);
            string searchColumn = "idSchedules";      //Columna para discriminar
            string searchState = "%";

            //Act
            string result = Schedules.Update_Schedule(idSchedule, bus_id, description, start_time, end_time, searchState);
            if (result.Equals("Exito"))
            {
                object[,] row_update = sch.SearchSchedule(idSchedule, searchColumn, searchState);
                
                if ((row_update[0,1].Equals(bus_id) ) &&
                    (row_update[0,2].Equals(description) ) &&
                    (row_update[0,3].Equals(start_time) ) &&
                    (row_update[0,4].Equals(end_time) ) 
                   )
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreEqual("Exito2", result);
        }

        // Pruebas de actualizacion de registro de horarios exitosa, validando datos
        [Test]
        public void Test3_UpdateSchedules2()
        {
            //Arrange
            int idSchedule = sch.Random_idSchedule(); //generara un random entre los id de horarios
            int bus_id = bus.Random_idBus(); //generara un random entre los id de buses
            string description = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            int start_time = rnd.Next(8, 22);//numero aleatorio para la hora de salida
            int end_time = start_time + rnd.Next(1, 3); //numero aleatorio entre 1 y 3 horas despues de la salida
            int state = rnd.Next(1, 2);
            string searchColumn = "idSchedules";      //Columna para discriminar
            string searchState = "%";

            //Act
            string result = Schedules.Update_Schedule(idSchedule, bus_id, description, start_time, end_time, searchState);
            if (result.Equals("Exito"))
            {
                object[,] row_update = sch.SearchSchedule(idSchedule, searchColumn, searchState);

                if ((row_update[0,1].Equals(bus_id.ToString())) &&
                    (row_update[0, 2].Equals(description.ToString())) &&
                    (row_update[0, 3].Equals(start_time.ToString())) &&
                    (row_update[0, 4].Equals(end_time.ToString())) 
                   )
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreNotEqual("Exito2", result);
        }

        // Pruebas de actualizacion de registro de horarios exitosa, validando datos
        [Test]
        public void Test3_UpdateSchedules3()
        {
            //Arrange
            int idSchedule = -5; //generara un random entre los id de horarios
            int bus_id = bus.Random_idBus(); //generara un random entre los id de buses
            string description = idSchedule + " description update:" + bus_id;
            int start_time = rnd.Next(8, 22);//numero aleatorio para la hora de salida
            int end_time = start_time + rnd.Next(1, 3); //numero aleatorio entre 1 y 3 horas despues de la salida
            int state = rnd.Next(1, 2);
            string searchColumn = "idSchedules";      //Columna para discriminar
            string searchState = "%";

            //Act
            string result = Schedules.Update_Schedule(idSchedule, bus_id, description, start_time, end_time, searchState);
            if (result.Equals("Exito"))
            {
                object[,] row_update = sch.SearchSchedule(idSchedule, searchColumn, searchState);

                if ((row_update[0, 1].Equals(bus_id.ToString())) &&
                    (row_update[0, 2].Equals(description.ToString())) &&
                    (row_update[0, 3].Equals(start_time.ToString())) &&
                    (row_update[0, 4].Equals(end_time.ToString()))
                   )
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreNotEqual("Exito2", result);
        }

        // Pruebas de actualizacion de estado de horarios exitosa, validando datos
        [Test]
        public void Test4_DisableSchedules1()
        {
            //Arrange
            int idSchedule = sch.Random_idSchedule(); //generara un random entre los id de horarios
            int state = 0;//inactivo
            string searchColumn = "idSchedules";      //Columna para discriminar
            string searchState = "0";

            //Act
            string result = Schedules.Change_State_Schedule(idSchedule, state);
            if (result.Equals("Exito"))
            {
                object[,] row_update = sch.SearchSchedule(idSchedule, searchColumn, searchState);

                if (row_update[0,5].Equals(state))
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreEqual("Exito2", result);
        }

        // Pruebas de actualizacion de estado de horarios fallida por estados
        [Test]
        public void Test4_DisableSchedules2()
        {
            //Arrange
            int idSchedule = -55; //generara un random entre los id de horarios
            int state = 0;//inactivo
            string searchColumn = "idSchedules";      //Columna para discriminar
            string searchState = "0";

            //Act
            string result = Schedules.Change_State_Schedule(idSchedule, state);
            if (result.Equals("Exito2"))
            {
                object[,] row_update = sch.SearchSchedule(idSchedule, searchColumn, searchState);

                if (row_update[0, 5].Equals(state.ToString()))
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreNotEqual("Exito2", result);
        }

        // Pruebas de actualizacion de estado de horarios fallida por estado, validando datos
        [Test]
        public void Test4_DisableSchedules3()
        {
            //Arrange
            int idSchedule = sch.Random_idSchedule(); //generara un random entre los id de horarios
            int state = 2;//inactivo
            string searchColumn = "idSchedules";      //Columna para discriminar
            string searchState = "0";

            //Act
            string result = Schedules.Change_State_Schedule(idSchedule, state);
            if (result.Equals("Exito2"))
            {
                object[,] row_update = sch.SearchSchedule(idSchedule, searchColumn, searchState);

                if (row_update[0, 5].Equals(state))
                {
                    result = "Exito2";
                }
            }

            //Assert
            Assert.AreNotEqual("Exito1", result);
        }

        // Pruebas de busqueda de horario por unidad de bus que tiene resultados
        [Test]
        public void Test5_SearchBusSchedule1()
        {
            //Arange
            int idSchedule = sch.Random_idSchedule(); //generara un random entre los id de horarios
            string searchColum1 = "idSchedules";
            string searchColum2 = "bus_id";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] randomShedule = sch.SearchSchedule(idSchedule, searchColum1, searchState);
            int idBus = (int)randomShedule[0, 1]; //Me aseguro que la prueba tenga resultado
            object[,] result = sch.SearchSchedule(idBus, searchColum2, searchState);

            //Assert
            Assert.AreNotEqual(false, result[0,0]);
        }

        // Pruebas de busqueda de horario por unidad de bus que no tiene resultados
        [Test]
        public void Test5_SearchBusSchedule2()
        {
            //Arange
            int idBus = 1;
            string searchColum2 = "bus_id";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] result = sch.SearchSchedule(idBus, searchColum2, searchState);

            //Assert
            Assert.AreEqual(false, result[0, 0]);
        }

        // Pruebas de busqueda de horario por unidad de bus que no exite
        [Test]
        public void Test5_SearchBusSchedule3()
        {
            //Arange
            int idBus = -5;
            string searchColum2 = "bus_id";      //Columna para discriminar
            string searchState = "1";

            //Act 
            object[,] result = sch.SearchSchedule(idBus, searchColum2, searchState);

            //Assert
            Assert.AreEqual(false, result[0, 0]);
        }
    }
}
