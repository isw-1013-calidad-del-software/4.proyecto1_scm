﻿using NUnit.Framework;
using ScheduleControlManagement.Logic;

namespace ScheduleControlManagement
{
    [TestFixture]
    class TestPartner
    {
        // Pruebas de registro de socio exitosa
        [Test]
        public void Test11_RecordPartner1()
        {
            //Arrange
            string firstname = "test1";
            string lastname = "test last name";
            string member_line = "test member line";

            int capacity = 45;
            string driver = "Driver test 1";
            string helper = "Helper test 1";

            bool result = false;

            //Act
            string result_partner = Partner.Insert_Partner(firstname, lastname, member_line);
            int partner_id = Partner.LastInsertedId;
            string result_bus = Bus.Insert_Bus(partner_id, capacity, driver, helper);

            //Assert
            if (result_bus == "Exito" && result_partner == "Exito")
            {
                result = true;
            }
            Assert.AreEqual(true, result);

        }

        // Pruebas de registro de socio fallida, por datos en blanco
        [Test]
        public void Test11_RecordPartner2()
        {
            //Arrange
            string firstname = "test2";
            string lastname = "";//valor en blanco
            string member_line = "test member line";

            int capacity = 45;
            string driver = "Driver test 2";
            string helper = "Helper test 2";

            bool result = false;

            //Act
            string result_partner = Partner.Insert_Partner(firstname, lastname, member_line);
            int partner_id = Partner.LastInsertedId;
            string result_bus = Bus.Insert_Bus(partner_id, capacity, driver, helper);

            //Assert
            if (result_bus == "Exito" && result_partner == "Exito")
            {
                result = true;
            }
            Assert.AreNotEqual(true, result);
        }

        // Pruebas de registro de socio fallida, por integridad de datos 
        [Test]
        public void Test11_RecordPartner3()
        {
            //Arrange
            string firstname = "test3";
            string lastname = "test last name3";
            string member_line = "test member line";

            int capacity = 45;
            string driver = "Driver test 3";
            string helper = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";//valor demaciado extenso

            bool result = false;

            //Act
            string result_partner = Partner.Insert_Partner(firstname, lastname, member_line);
            int partner_id = Partner.LastInsertedId;
            string result_bus = Bus.Insert_Bus(partner_id, capacity, driver, helper);

            //Assert
            if (result_bus == "Exito" && result_partner == "Exito")
            {
                result = true;
            }
            Assert.AreNotEqual(true, result);
        }
    }
}
